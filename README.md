# Nubank Mobile Hiring Exercise 01/11/2016 #

* Project Name: NubankDemo
* Project Owner: Gabriel L Ferreira
* Device used: Nexus 5x Android 7.0
* Android Studio build 2.2.2
* MinSdkVersion 18 - TargetSdkVersion 25
* GradleVersion 2.2.2

## Run ##

1. Connect your device and enable USB debug mode
2. Execute commands below:

```
#!script

$ gradlew installDebug
$ adb shell am start -n "br.com.gabrielferreira.nubankdemo/br.com.gabrielferreira.nubankdemo.activity.main.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
```



## Testing ##
### For testing purpose it's require to disable all Android animations
1. Connect your device and enable USB debug mode
2. Execute commands below to disable Android animations

```
#!script

$ adb shell settings put global window_animation_scale 0 
$ adb shell settings put global transition_animation_scale 0  
$ adb shell settings put global animator_duration_scale 0
```
     
3. Execute commands below for build and testing:


```
#!script
$ gradlew installDebug
$ adb shell am instrument -w -r -e debug false -e class br.com.gabrielferreira.nubankdemo.activity.main.MainPresenterTest br.com.gabrielferreira.nubankdemo.test/android.support.test.runner.AndroidJUnitRunner
```



## Open-source libraries ##
* Crashlytics pack 2.6.5
* Calligraphy 2.1.0
* Butterknife 7.0.1
* Ion 2
* Gson 2.4
* Junit 4.12
* Espresso 2.2
* Hamcrest 1.3 - library of matchers