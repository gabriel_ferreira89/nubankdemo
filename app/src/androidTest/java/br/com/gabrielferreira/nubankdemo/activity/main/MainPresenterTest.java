package br.com.gabrielferreira.nubankdemo.activity.main;


import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import junit.framework.Assert;

import org.hamcrest.Matchers;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;
import br.com.gabrielferreira.nubankdemo.model.HalResource;
import br.com.gabrielferreira.nubankdemo.model.Notice;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.IonIdlingResource;
import br.com.gabrielferreira.nubankdemo.util.TestUtils;
import br.com.gabrielferreira.nubankdemo.util.Utils;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainPresenterTest {

    private IonIdlingResource ionIdlingResource;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void registerIntentServiceIdlingResource() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        ionIdlingResource = new IonIdlingResource(getClass().getName(), instrumentation.getTargetContext());
        Espresso.registerIdlingResources(ionIdlingResource);
    }

    @Test
    public void mainActivityTest() throws InterruptedException, IOException, JSONException {

        HalResource halResource = (HalResource) Utils.getMockedJson(R.raw.main_mock_20161025, HalResource.class);
        Assert.assertNotNull(halResource);
        Assert.assertNotNull(halResource.getLinkMap());
        Assert.assertNotSame(halResource.getLinkMap().size(), 0);
    }

    @Test
    public void noticeDialogFragmentTest() throws InterruptedException, IOException, JSONException {

        Notice noticeObj = (Notice) Utils.getMockedJson(R.raw.notice_mock_20161025, Notice.class);
        Assert.assertNotNull(noticeObj);
        Assert.assertNotNull(noticeObj.getLinkMap());
        Assert.assertNotSame(noticeObj.getLinkMap().size(), 0);

        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround

        ViewInteraction textView = onView(allOf(withId(R.id.textview_notice_dialog_title), isDisplayed()));
        textView.check(matches(withText(noticeObj.getTitle())));

        ViewInteraction textView2 = onView(allOf(withId(R.id.textview_notice_dialog_description), isDisplayed()));
        textView2.check(matches(withText(Utils.HtmlLtoSpanned(noticeObj.getDescription()).toString())));

        ViewInteraction button = onView(allOf(withId(R.id.button_notice_dialog_second_action), isDisplayed()));
        button.check(matches(withText(noticeObj.getSecondaryAction().getTitle())));

        ViewInteraction button2 = onView(allOf(withId(R.id.button_notice_dialog_first_action), isDisplayed()));
        button2.check(matches(withText(noticeObj.getPrimaryAction().getTitle())));
    }

    @Test
    public void chargeBackDialogFragmentTest() throws InterruptedException, IOException, JSONException {

        Chargeback chargebackObj = (Chargeback) Utils.getMockedJson(R.raw.chargeback_mock_20161025, Chargeback.class);
        Assert.assertNotNull(chargebackObj);
        Assert.assertNotNull(chargebackObj.getLinkMap());
        Assert.assertNotSame(chargebackObj.getLinkMap().size(), 0);
        Assert.assertNotNull(chargebackObj.getReasonDetails());
        Assert.assertNotSame(chargebackObj.getReasonDetails().size(), 0);
        Assert.assertEquals(chargebackObj.getReasonDetails().size(), 2);

        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.button_notice_dialog_first_action), isDisplayed()));
        appCompatButton.perform(scrollTo(), click());

        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround

        ViewInteraction textView = onView(
                allOf(withId(R.id.textview_chargeback_title), isDisplayed()));
        textView.check(matches(withText(chargebackObj.getTitle())));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.textview_chargeback_lock_card), isDisplayed()));
        textView2.check(matches(withText(R.string.chargeback_locked_card)));

        ViewInteraction switch_ = onView(
                allOf(withId(R.id.switchcompat_chargeback_merchant), isDisplayed()));
        switch_.check(matches(withText(chargebackObj.getReasonDetails().get(0).getTitle())));

        ViewInteraction switch_2 = onView(
                allOf(withId(R.id.switchcompat_chargeback_card_possession), isDisplayed()));
        switch_2.check(matches(withText(chargebackObj.getReasonDetails().get(1).getTitle())));

        ViewInteraction editText = onView(
                allOf(withId(R.id.edittext_chargeback_comment), isDisplayed()));
        editText.check(matches(Matchers.anything()));

        ViewInteraction button = onView(
                allOf(withId(R.id.button_chargeback_dialog_cancel), isDisplayed()));
        button.check(matches(isDisplayed()));
        button.check(matches(isEnabled()));

        ViewInteraction button2 = onView(
                allOf(withId(R.id.button_chargeback_dialog_contest), isDisplayed()));
        button2.check(matches(isDisplayed()));
        button2.check(matches(not(isEnabled())));
    }

    @Test
    public void chargebackProcessExecution() throws InterruptedException {

        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.button_notice_dialog_first_action), isDisplayed()));
        appCompatButton.perform(scrollTo(), click());

        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround

        ViewInteraction textView = onView(
                allOf(withId(R.id.textview_chargeback_lock_card), isDisplayed()));
        textView.perform(click());

        TestUtils.performSleepThread();

        textView.check(matches(withText(R.string.chargeback_unlocked_card)));

        ViewInteraction editText = onView(
                allOf(withId(R.id.edittext_chargeback_comment), isDisplayed()));
        editText.perform(ViewActions.typeText("teste"));

        closeSoftKeyboard();

        ViewInteraction button2 = onView(
                allOf(withId(R.id.button_chargeback_dialog_contest), isDisplayed()));
        button2.check(matches(isDisplayed()));
        button2.check(matches(isEnabled()));
        button2.perform(click());

        TestUtils.performSleepThread();

        ViewInteraction textView2 = onView(
                allOf(withText(R.string.chargeback_success_title), isDisplayed()));
        textView2.check(matches(withText(R.string.chargeback_success_title)));
    }


    @After
    public void unregisterIntentServiceIdlingResource() {
        Espresso.unregisterIdlingResources(ionIdlingResource);
    }
}