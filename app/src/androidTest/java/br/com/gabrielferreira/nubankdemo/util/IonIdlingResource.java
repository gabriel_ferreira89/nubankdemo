package br.com.gabrielferreira.nubankdemo.util;

/**
 * Created on 31/10/2016.
 */

import android.content.Context;
import android.support.test.espresso.IdlingResource;
import android.util.Log;

import com.koushikdutta.ion.Ion;

/**
 * Espresso IdlingResource for Ion
 * https://github.com/koush/ion
 * https://code.google.com/p/android-test-kit/
 * Because you can't deregister an IdlingResource, you have to create a new one per test.
 * You need to use a unique resourceName each test.
 */
public final class IonIdlingResource implements IdlingResource {

    private static final String TAG = "IonIdlingResource";
    private final String resourceName;
    private Context context;
    private volatile ResourceCallback resourceCallback;


    public IonIdlingResource(String resourceName, Context context) {
        this.resourceName = resourceName;
        this.context = context;
    }

    @Override
    public String getName() {
        return resourceName;
    }

    @Override
    public boolean isIdleNow() {
        int count = Ion.getDefault(context).getPendingRequestCount(context);
        if(count == 0) {
            resourceCallback.onTransitionToIdle();
        }
        Log.d(TAG,resourceName + " count=" + count);
        return count == 0;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}