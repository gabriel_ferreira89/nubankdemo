package br.com.gabrielferreira.nubankdemo.util;

/**
 * Created on 01/11/2016.
 */

public class TestUtils {

    /**
     * This method is necessary because IonIdlingResource was not sufficient for the
     * DialogFragment creation delay.
     * Based on further searches, its known that Espresso 2.2.0 its not capable yet to handle
     * DialogFragments properly.
     * Based on that, it was decided to make the Thread sleep for the same amount of time as
     * Ion has for timeout.
     * @throws InterruptedException
     */

    public static void performSleepThread() throws InterruptedException {
        Thread.sleep(Constants.WS_DEFAULT_TIMEOUT); //DialogFragment and WS workaround
    }
}