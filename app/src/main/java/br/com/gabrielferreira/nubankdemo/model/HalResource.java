package br.com.gabrielferreira.nubankdemo.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;

/**
 * Created on 25/10/2016.
 */

public class HalResource implements Serializable{

    public HalResource() {
        linkMap = new HashMap<>();
    }

    private HashMap<String, Link> linkMap;

    public Map<String, Link> getLinkMap() {
        return linkMap;
    }

    public void setLinkMap(JsonObject linksSerialized) {
        this.linkMap = parseLinkMap(linksSerialized);
    }

    public static class HalResourceDeserializerFromJson implements JsonDeserializer<HalResource> {

        @Override
        public HalResource deserialize
                (JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            HalResource halResource = new HalResource();
            JsonObject jObject = json.getAsJsonObject();
            halResource.setLinkMap(jObject.getAsJsonObject(Constants.WS_FIELD_LINKS));
            return halResource;
        }
    }

    public HashMap<String, Link> parseLinkMap(JsonObject linksSerialized) {
        HashMap<String, Link> hashMap = new HashMap<>();
        try {
            hashMap = Utils.jsonToMap(linksSerialized);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hashMap;
    }
}
