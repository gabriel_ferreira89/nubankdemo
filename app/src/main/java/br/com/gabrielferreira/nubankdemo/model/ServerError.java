package br.com.gabrielferreira.nubankdemo.model;

import com.google.gson.JsonObject;

public class ServerError {

    private Integer code;
    private String message;

    private static final String MESSAGE_KEY = "message";
    private static final String CODE_KEY = "code";

    public ServerError () {}

    public ServerError (Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ServerError (JsonObject serverError){
        this.code = serverError.get(CODE_KEY).isJsonNull() ? -1 : serverError.get(CODE_KEY).getAsInt();
        this.message = serverError.get(MESSAGE_KEY).isJsonNull() ? "" : serverError.get(MESSAGE_KEY).getAsString();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
