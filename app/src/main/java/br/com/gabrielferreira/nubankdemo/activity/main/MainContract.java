package br.com.gabrielferreira.nubankdemo.activity.main;

import br.com.gabrielferreira.nubankdemo.activity.base.BasePresenter;
import br.com.gabrielferreira.nubankdemo.activity.base.BaseView;
import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;

/**
 * Created on 25/10/2016.
 * This specifies the contract between the view and the presenter.
 */

public interface MainContract {

    interface View extends BaseView<Presenter>{

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void dialogFlowSwitch(String key, String url);

        void executeChargeback(Chargeback chargeback, String url);

        void executeLockCard(final ServerResponseManager listener, String url);
    }
}
