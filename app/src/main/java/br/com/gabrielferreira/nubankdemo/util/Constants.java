package br.com.gabrielferreira.nubankdemo.util;

/**
 * Created on 25/10/2016.
 */

public class Constants {

    public static final String BASE_URL = "https://nu-mobile-hiring.herokuapp.com/";

    public static final String LINK_MAIN = "main";
    public static final String LINK_NOTICE = "notice";
    public static final String LINK_CHARGEBACK = "chargeback";
    public static final String LINK_BLOCK_CARD = "block_card";
    public static final String LINK_UNBLOCK_CARD = "unblock_card";
    public static final String LINK_EXIT = "exit";
    public static final String LINK_STATUS = "status";
    public static final String LINK_STATUS_OK = "ok";
    public static final String LINK_SELF = "self";


    public static final String EXTRA_NOTICE = "EXTRA_NOTICE";
    public static final String EXTRA_CHARGEBACK = "EXTRA_CHARGEBACK";


    public static final String WS_FIELD_LINKS = "links";
    public static final String WS_FIELD_CHARGEBACK_MERCHANT = "merchant_recognized";
    public static final String WS_FIELD_CHARGEBACK_CARD = "card_in_possession";

    /**
     * Business Rules
     */
    public static final boolean CHARGEBACK_CARD_INITIAL_STATE = false; //false->unlocked true->locked
    public static final int WS_DEFAULT_TIMEOUT = 10000;

    /* WEB SERVICE BODY CODE */
    public static final class GSErrorCode {
        /********** GENERIC ERRORS **********/
        public static final int GENERIC_FAIL_PARSING_JSON = 1;                      /* HTTP ERROR CODE 400 */
        /********** AUTH ERRORS **********/
        public static final int AUTH_UNREGISTERED_SOCIAL_NETWORK = 3;               /* HTTP ERROR CODE 400 */
        public static final int AUTH_UNAUTHENTICATED_USER = 16;                       /* HTTP ERROR CODE 401 */
        public static final int AUTH_INVALID_TOKEN = 10;                            /* HTTP ERROR CODE 401 */
        /* AUTH FACEBOOK ERRORS */
        public static final int AUTH_FACEBOOK_MISSING_TOKEN = 1;                    /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_APP_ID = 13;                  /* HTTP ERROR CODE 403 */
        public static final int AUTH_FACEBOOK_UNAUTHORIZED = 458;                   /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_SESSION = 463;                /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_TOKEN = 190;                  /* HTTP ERROR CODE 400 */

        public static final int CONFLICT = 8;                                       /* HTTP ERROR CODE 409 */
        public static final int VALIDATION_ERROR = 13;
        public static final int FIELD_VALIDATION_ERROR = 2;
        public static final int INVALID_HEADER = 6;
        public static final int INVALID_PARAM = 4;
    }

    /* WEB SERVICE HEADER CODE */
    public static final class WSHeaderCode {
        public static final int SUCCESS_CODE = 200;         //    200   Success
        public static final int OBJECT_CREATED_CODE = 201;  //    201   Object created
        public static final int BAD_REQUEST_CODE = 400;     //    400	Bad Request: missing parameters or bad message format
        public static final int UNAUTHORIZED_CODE = 401;    //    401	Unauthorized: must be auth
        public static final int FORBIDDEN_CODE = 403;       //    403	Forbidden: role error
        public static final int NOT_FOUND_CODE = 404;       //    404	Not found: Object not found
        public static final int CONFLICT_CODE = 409;        //    409	Conflict: Object already exist
        public static final int INTERNAL_ERROR_CODE = 500;        //    500	Internal server error

        public static final int TIME_OUT_CODE = 503;        //    504   Timeout error
        public static final int NO_CONNECTION_CODE = 5000;  //    5000  No connection error
        public static final int GENERIC_EXCEPTION = 6000;   //    6000  Generic exception error
    }

    /* WEB SERVICE BODY CODE */
    public static final class WSErrorCode {
        /********** GENERIC ERRORS **********/
        public static final int GENERIC_FAIL_PARSING_JSON = 1;                      /* HTTP ERROR CODE 400 */
        public static final int NO_CONNECTION = 1;                                  /* HTTP ERROR CODE 5000 */
        /********** AUTH ERRORS **********/
        public static final int AUTH_UNREGISTERED_SOCIAL_NETWORK = 3;               /* HTTP ERROR CODE 400 */
        public static final int AUTH_FAIL_CREATING_OBJECT = 5;                      /* HTTP ERROR CODE 500 */
        public static final int AUTH_UNAUTHENTICATED_USER = 16;                       /* HTTP ERROR CODE 401 */
        public static final int AUTH_INVALID_TOKEN = 10;                            /* HTTP ERROR CODE 401 */
        /* AUTH LOGIN ERRORS */
        public static final int AUTH_LOGIN_UNCOFIRMATED_ACCOUNT = 11;               /* HTTP ERROR CODE 401 */
        public static final int AUTH_LOGIN_INVALID_INFORMATION = 12;                /* HTTP ERROR CODE 401 */
        /* AUTH LOGOUT ERRORS */
        public static final int AUTH_LOGOUT_INEXISTENT_TOKEN = 3;                   /* HTTP ERROR CODE 404 */
        public static final int AUTH_LOGOUT_MISSING_AUTHORIZATION_HEADER = 6;       /* HTTP ERROR CODE 401 */
        /* AUTH FACEBOOK ERRORS */
        public static final int AUTH_FACEBOOK_MISSING_TOKEN = 1;                    /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_APP_ID = 13;                  /* HTTP ERROR CODE 403 */
        public static final int AUTH_FACEBOOK_UNAUTHORIZED = 458;                   /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_SESSION = 463;                /* HTTP ERROR CODE 400 */
        public static final int AUTH_FACEBOOK_INVALID_TOKEN = 190;                  /* HTTP ERROR CODE 400 */
        /* AUTH GOOGLE ERRORS */
        public static final int AUTH_GOOGLE_MISSING_TOKEN = 1;                      /* HTTP ERROR CODE 400 */
        public static final int AUTH_GOOGLE_INVALID_APP_ID = 16;                      /* HTTP ERROR CODE 400 */
        public static final int AUTH_GOOGLE_INVALID_TOKEN = 13;                     /* HTTP ERROR CODE 400 */

        /********** USER AND PASSWORD ERRORS **********/
        public static final int USER_MISSING_USERNAME = 1;                          /* HTTP ERROR CODE 400 */
        public static final int USER_FAIL_VALIDATING = 2;                           /* HTTP ERROR CODE 400 */
        public static final int USER_NOT_FOUND = 3;                                 /* HTTP ERROR CODE 404 */
        public static final int USER_FAIL_CREATING = 5;                             /* HTTP ERROR CODE 500 */
        public static final int USER_ALREADY_EXISTS = 8;                            /* HTTP ERROR CODE 409 */
        public static final int USER_INVALID_EMAIL = 13;                            /* HTTP ERROR CODE 400 */
        public static final int USER_INVALID_PASSWORD = 13;                            /* HTTP ERROR CODE 400 */
        public static final int USER_FAIL_SENDING_EMAIL = 15;                       /* HTTP ERROR CODE 500 */

        /*********  CREATE FARM ************/
        public static final int CREATE_FARM_MAX_NUMBER = 13;                            /* HTTP ERROR CODE 400 */
    }
}
