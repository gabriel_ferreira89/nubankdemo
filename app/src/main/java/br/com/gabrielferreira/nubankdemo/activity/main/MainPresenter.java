package br.com.gabrielferreira.nubankdemo.activity.main;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.google.gson.GsonBuilder;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.activity.chargeback.ChargebackDialogFragment;
import br.com.gabrielferreira.nubankdemo.activity.notice.NoticeDialogFragment;
import br.com.gabrielferreira.nubankdemo.helper.ServiceErrorDialogHelper;
import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;
import br.com.gabrielferreira.nubankdemo.model.HalResource;
import br.com.gabrielferreira.nubankdemo.model.Notice;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.service.WSNubank;
import br.com.gabrielferreira.nubankdemo.service.WSNubankChargeback;
import br.com.gabrielferreira.nubankdemo.service.WSNubankNotice;
import br.com.gabrielferreira.nubankdemo.util.ActivityUtils;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created on 25/10/2016.
 * Listens to user actions from the UI, retrieves the data and updates
 * the UI as required.
 */

class MainPresenter implements MainContract.Presenter{


    private final MainActivity mainActivity;
    private final FragmentManager fragmentManager;


    MainPresenter(@NonNull MainActivity mainActivity) {
        this.mainActivity = checkNotNull(mainActivity);
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }


    @Override
    public void start() {
        dialogFlowSwitch(Constants.LINK_MAIN, "");
    }

    public void dialogFlowSwitch(String key, String url){

        switch (key){
            case Constants.LINK_MAIN:
                WSNubank.getInstance(mainActivity).retrieveMain(new ServerResponseManager() {
                    @Override
                    public void onSuccess(Object data) {
                        if (data instanceof HalResource && ((HalResource)data).getLinkMap().containsKey(Constants.LINK_NOTICE)){
                                dialogFlowSwitch(Constants.LINK_NOTICE, ((HalResource) data).getLinkMap().get(Constants.LINK_NOTICE).getHref());
                        }else{
                            if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                                ServiceErrorDialogHelper.ExitErrorDialog(mainActivity, new ResponseError()).show();
                            }
                        }
                    }
                    @Override
                    public void onError(ResponseError data) {
                        if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                            ServiceErrorDialogHelper.ExitErrorDialog(mainActivity, data).show();
                        }
                    }
                }, Constants.BASE_URL);
                break;
            case Constants.LINK_NOTICE:
                WSNubankNotice.getInstance(mainActivity).retrieveNotice(new ServerResponseManager() {
                    @Override
                    public void onSuccess(Object data) {
                        if (data instanceof Notice){
                            openNoticeDialogFragment((Notice) data);
                        }else{
                            if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                                ServiceErrorDialogHelper.ExitErrorDialog(mainActivity, new ResponseError()).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ResponseError data) {
                        if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                            ServiceErrorDialogHelper.ExitErrorDialog(mainActivity, data).show();
                        }
                    }
                }, url);
                break;
            case Constants.LINK_CHARGEBACK :
                mainActivity.showFullScreenProgressDialog();
                WSNubankChargeback.getInstance(mainActivity).retrieveChargebackOperation(new ServerResponseManager() {
                    @Override
                    public void onSuccess(Object data) {
                        mainActivity.dismissFullScreenProgressDialog();
                        if (data instanceof Chargeback){
                            mainActivity.dismissFullScreenProgressDialog();
                            openChargebackDialogFragment((Chargeback) data);
                        }
                    }

                    @Override
                    public void onError(ResponseError data) {
                        mainActivity.dismissFullScreenProgressDialog();
                        if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                            ServiceErrorDialogHelper.GenericErrorDialog(mainActivity, data).show();
                        }
                    }
                }, url);
                break;
            case Constants.LINK_EXIT:
                mainActivity.showOkAlertDialog("O aplicativo demo será fechado", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mainActivity.onBackPressed();
                    }
                });
                break;
            default:

                break;
        }
    }

    private void openNoticeDialogFragment(Notice notice){
        NoticeDialogFragment noticeDialogFragment = (NoticeDialogFragment) fragmentManager.findFragmentByTag(NoticeDialogFragment.TAG);
        if (noticeDialogFragment == null){
            noticeDialogFragment = NoticeDialogFragment.newInstance(notice);
            ActivityUtils.openDialogFragment(fragmentManager, noticeDialogFragment, NoticeDialogFragment.TAG);
            noticeDialogFragment.setPresenter(this);
        }
    }

    private void openChargebackDialogFragment(Chargeback chargeback){
        ChargebackDialogFragment chargebackDialogFragment = (ChargebackDialogFragment) fragmentManager.findFragmentByTag(ChargebackDialogFragment.TAG);
        if (chargebackDialogFragment == null){
            chargebackDialogFragment = ChargebackDialogFragment.newInstance(chargeback);
            ActivityUtils.openDialogFragment(fragmentManager, chargebackDialogFragment, ChargebackDialogFragment.TAG);
            chargebackDialogFragment.setPresenter(this);
        }
    }

    public void executeChargeback(Chargeback chargeback, String url){
        final String title = mainActivity.getString(R.string.chargeback_success_title);
        final String description = mainActivity.getString(R.string.chargeback_success_desc);

        mainActivity.showFullScreenProgressDialog();
        WSNubankChargeback.getInstance(mainActivity).postChargebackOperation(new ServerResponseManager() {
            @Override
            public void onSuccess(Object data) {
                mainActivity.dismissFullScreenProgressDialog();
                ChargebackDialogFragment chargebackDialogFragment = (ChargebackDialogFragment) fragmentManager.findFragmentByTag(ChargebackDialogFragment.TAG);
                if (chargebackDialogFragment != null){
                    chargebackDialogFragment.dismiss();
                }
                NoticeDialogFragment noticeDialogFragment = (NoticeDialogFragment) fragmentManager.findFragmentByTag(NoticeDialogFragment.TAG);
                if (noticeDialogFragment != null){
                    noticeDialogFragment.dismiss();
                }
                mainActivity.showOkCustomDialog(title, description, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainActivity.onBackPressed();
                    }
                });
            }

            @Override
            public void onError(ResponseError data) {
                mainActivity.dismissFullScreenProgressDialog();
                if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                    ServiceErrorDialogHelper.GenericErrorDialog(mainActivity, data).show();
                }
            }
        }, url, chargeback);
    }

    public void executeLockCard(final ServerResponseManager listener, String url){
        mainActivity.showFullScreenProgressDialog();
        WSNubankChargeback.getInstance(mainActivity).postLockCardOperation(new ServerResponseManager() {
            @Override
            public void onSuccess(Object data) {
                mainActivity.dismissFullScreenProgressDialog();
                listener.onSuccess(data);
            }

            @Override
            public void onError(ResponseError data) {
                mainActivity.dismissFullScreenProgressDialog();
                listener.onError(null);
                if (!mainActivity.isDestroyed() && !mainActivity.isFinishing()){
                    ServiceErrorDialogHelper.GenericErrorDialog(mainActivity, data).show();
                }
            }
        }, url);
    }
}
