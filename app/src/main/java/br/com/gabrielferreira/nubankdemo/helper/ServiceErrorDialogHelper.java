package br.com.gabrielferreira.nubankdemo.helper;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.List;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.model.ServerError;
import br.com.gabrielferreira.nubankdemo.util.Constants;


public class ServiceErrorDialogHelper {
    private static DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };
    private static DialogInterface.OnClickListener exitListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            System.exit(0);
        }
    };

    public static AlertDialog GenericErrorDialog(final Context context, ResponseError error){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        String errorMessage = "";
        DialogInterface.OnClickListener logoutListener = null;

        /* Handling Empty or Null Error Array. */
        if(error.getServerErrorList() == null || error.getServerErrorList().size() <= 0){
            errorMessage = context.getResources().getString(R.string.generic_error);
            builder.setMessage(errorMessage);
            builder.setNegativeButton(context.getResources().getString(R.string.ok), okListener);
            return builder.create();
        }

        /* Handling Existing Error Array. */
        List<ServerError> errorList = error.getServerErrorList();
        switch (error.getStatusCode()){
            case Constants.WSHeaderCode.NO_CONNECTION_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSErrorCode.NO_CONNECTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.INTERNAL_ERROR_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";

                    errorMessage += context.getResources().getString(R.string.server_internal_error);
                }
                break;
            case Constants.WSHeaderCode.TIME_OUT_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSHeaderCode.TIME_OUT_CODE || errorList.get(i).getCode() == Constants.WSErrorCode.NO_CONNECTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.GENERIC_EXCEPTION:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSHeaderCode.GENERIC_EXCEPTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.BAD_REQUEST_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";

                    errorMessage += context.getResources().getString(R.string.bad_request);
                }
                break;
        }
        builder.setMessage(errorMessage);
        if(logoutListener != null) {
            builder.setCancelable(false);
            builder.setNegativeButton(context.getResources().getString(R.string.ok), logoutListener);
        } else {
            builder.setNegativeButton(context.getResources().getString(R.string.ok), okListener);
        }
        return builder.create();
    }

    public static AlertDialog ExitErrorDialog(final Context context, ResponseError error){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        String errorMessage = "";
        DialogInterface.OnClickListener logoutListener = null;

        /* Handling Empty or Null Error Array. */
        if(error.getServerErrorList() == null || error.getServerErrorList().size() <= 0){
            errorMessage = context.getResources().getString(R.string.generic_error_exit);
            builder.setMessage(errorMessage);
            builder.setNegativeButton(context.getResources().getString(R.string.ok), exitListener);
            return builder.create();
        }

        /* Handling Existing Error Array. */
        List<ServerError> errorList = error.getServerErrorList();
        switch (error.getStatusCode()){
            case Constants.WSHeaderCode.NO_CONNECTION_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSErrorCode.NO_CONNECTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.INTERNAL_ERROR_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";

                    errorMessage += context.getResources().getString(R.string.server_internal_error);
                }
                break;
            case Constants.WSHeaderCode.TIME_OUT_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSHeaderCode.TIME_OUT_CODE || errorList.get(i).getCode() == Constants.WSErrorCode.NO_CONNECTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.GENERIC_EXCEPTION:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";
                    if(errorList.get(i).getCode() == Constants.WSHeaderCode.GENERIC_EXCEPTION){
                        errorMessage += errorList.get(i).getMessage();
                    }
                }
                break;
            case Constants.WSHeaderCode.BAD_REQUEST_CODE:
                for (int i = 0; i < errorList.size(); i++) {
                    if ((i != errorList.size() - 1) && (errorList.size() > 1))
                        errorMessage += "\n";

                    errorMessage += context.getResources().getString(R.string.bad_request);
                }
                break;
        }
        builder.setMessage(errorMessage);
        if(logoutListener != null) {
            builder.setCancelable(false);
            builder.setNegativeButton(context.getResources().getString(R.string.ok), logoutListener);
        } else {
            builder.setNegativeButton(context.getResources().getString(R.string.ok), exitListener);
        }
        return builder.create();
    }
}
