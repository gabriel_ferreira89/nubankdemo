package br.com.gabrielferreira.nubankdemo.listener;

import br.com.gabrielferreira.nubankdemo.model.ResponseError;

public interface ServerResponseManager {

    void onSuccess(Object data);
    void onError(ResponseError data);
}
