package br.com.gabrielferreira.nubankdemo.activity.main;

import android.os.Bundle;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.activity.base.BaseActivity;
import br.com.gabrielferreira.nubankdemo.activity.chargeback.ChargebackDialogFragment;
import br.com.gabrielferreira.nubankdemo.activity.notice.NoticeDialogFragment;

/**
 * Created on 25/10/2016.
 */

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new MainPresenter(this).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ChargebackDialogFragment chargebackDialogFragment = (ChargebackDialogFragment) getSupportFragmentManager().findFragmentByTag("ChargebackDialogFragment");
        NoticeDialogFragment noticeDialogFragment = (NoticeDialogFragment) getSupportFragmentManager().findFragmentByTag("NoticeDialogFragment");
        if (noticeDialogFragment == null && chargebackDialogFragment == null){
            finish();
        }
    }
}
