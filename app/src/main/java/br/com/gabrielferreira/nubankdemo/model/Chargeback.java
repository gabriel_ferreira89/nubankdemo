package br.com.gabrielferreira.nubankdemo.model;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.gabrielferreira.nubankdemo.util.Constants;

/**
 * Created on 25/10/2016.
 */
public class Chargeback extends HalResource{

    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("comment_hint")
    private String commentHint;
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("autoblock")
    private boolean autoblock;
    @SerializedName("reason_details")
    @Expose
    private List<ReasonDetail> reasonDetails = new ArrayList<ReasonDetail>();

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentHint() {
        return commentHint;
    }

    public void setCommentHint(String commentHint) {
        this.commentHint = commentHint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isAutoblock() {
        return autoblock;
    }

    public void setAutoblock(boolean autoblock) {
        this.autoblock = autoblock;
    }

    public List<ReasonDetail> getReasonDetails() {
        return reasonDetails;
    }

    public ReasonDetail getReasonDetail(String id){
        if (id == null || "".equals(id)){
            return null;
        }
        for (ReasonDetail reason: this.reasonDetails) {
            if (id.contains(reason.getId())){
                return reason;
            }
        }
        return null;
    }

    public void setReasonDetails(List<ReasonDetail> reasonDetails) {
        this.reasonDetails = reasonDetails;
    }

    public static class ChargebackDeserializerFromJson implements JsonDeserializer<Chargeback> {

        @Override
        public Chargeback deserialize
                (JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            Chargeback chargeback = new Gson().fromJson(json, Chargeback.class);
            JsonObject jObject = json.getAsJsonObject();
            chargeback.setLinkMap(jObject.getAsJsonObject(Constants.WS_FIELD_LINKS));
            return chargeback;
        }
    }
}
