package br.com.gabrielferreira.nubankdemo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.gson.GsonBuilder;
import com.koushikdutta.ion.Ion;

import br.com.gabrielferreira.nubankdemo.util.Utils;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created on 25/10/2016.
 */

public class NubankdemoApplication extends Application {

    private static NubankdemoApplication instance;

    public static NubankdemoApplication getInstance() {
        if (instance == null){
            instance = new NubankdemoApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        configureCalligraphy();
        initIon();
    }

    private void initIon() {
        GsonBuilder gsonBldr = Utils.getStandardGsonBuilder();
        Ion.getDefault(instance).configure().setGson(gsonBldr.create());
    }

    private void configureCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
