package br.com.gabrielferreira.nubankdemo.activity.notice;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.activity.base.BaseDialogFragment;
import br.com.gabrielferreira.nubankdemo.activity.main.MainContract;
import br.com.gabrielferreira.nubankdemo.model.Notice;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoticeDialogFragment extends BaseDialogFragment implements MainContract.View{

    public static final String TAG = "NoticeDialogFragment";

    @Bind(R.id.textview_notice_dialog_title)
    TextView titleTextView;
    @Bind(R.id.textview_notice_dialog_description)
    TextView descriptionTextView;
    @Bind(R.id.button_notice_dialog_first_action)
    Button firstActionButton;
    @Bind(R.id.button_notice_dialog_second_action)
    Button secondActionButton;

    private MainContract.Presenter presenter;
    private Notice noticeModel;

    public static NoticeDialogFragment newInstance(Notice notice) {
        NoticeDialogFragment fragment = new NoticeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_NOTICE, notice);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    presenter.dialogFlowSwitch(Constants.LINK_EXIT, "");
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notice_dialog, container, false);
        noticeModel = (Notice) getArguments().getSerializable(Constants.EXTRA_NOTICE);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        titleTextView.setText(noticeModel.getTitle());
        descriptionTextView.setText(Utils.HtmlLtoSpanned(noticeModel.getDescription()));
        firstActionButton.setText(noticeModel.getPrimaryAction().getTitle());
        firstActionButton.setTag(noticeModel.getPrimaryAction().getAction());
        secondActionButton.setText(noticeModel.getSecondaryAction().getTitle());
        secondActionButton.setTag(noticeModel.getSecondaryAction().getAction());
    }

    @OnClick(R.id.button_notice_dialog_second_action)
    void onCancelButtonClick(){
        presenter.dialogFlowSwitch(Constants.LINK_EXIT, "");
        dismiss();
    }

    @OnClick(R.id.button_notice_dialog_first_action)
    void onContinueButtonClick(){
        presenter.dialogFlowSwitch(Constants.LINK_CHARGEBACK, noticeModel.getLinkMap().get(Constants.LINK_CHARGEBACK).getHref());
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
