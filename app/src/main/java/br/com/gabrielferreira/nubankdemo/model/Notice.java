package br.com.gabrielferreira.nubankdemo.model;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

import br.com.gabrielferreira.nubankdemo.util.Constants;

/**
 * Created on 25/10/2016.
 */

public class Notice extends HalResource {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("primary_action")
    @Expose
    private Action primaryAction;
    @SerializedName("secondary_action")
    @Expose
    private Action secondaryAction;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Action getPrimaryAction() {
        return primaryAction;
    }

    public void setPrimaryAction(Action primaryAction) {
        this.primaryAction = primaryAction;
    }

    public Action getSecondaryAction() {
        return secondaryAction;
    }

    public void setSecondaryAction(Action secondaryAction) {
        this.secondaryAction = secondaryAction;
    }

    public static class NoticeDeserializerFromJson implements JsonDeserializer<Notice> {

        @Override
        public Notice deserialize
                (JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            Notice notice = new Gson().fromJson(json, Notice.class);
            JsonObject jObject = json.getAsJsonObject();
            notice.setLinkMap(jObject.getAsJsonObject(Constants.WS_FIELD_LINKS));
            return notice;
        }
    }
}
