package br.com.gabrielferreira.nubankdemo.activity.base;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import br.com.gabrielferreira.nubankdemo.R;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created on 25/10/2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected Context context;
    private AlertDialog alertDialog;
    private ProgressDialog fullscreenProgressDialog;

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showOkAlertDialog(String msg, DialogInterface.OnClickListener onClickListener) {
        if (context == null || "".equals(msg)){
            return;
        }
        if(alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", onClickListener)
                .show();
    }

    public void showFullScreenProgressDialog(){
        if (context == null){
            return;
        }
        if(fullscreenProgressDialog != null) {
            fullscreenProgressDialog.dismiss();
            fullscreenProgressDialog = null;
        }
        fullscreenProgressDialog = new ProgressDialog(context, R.style.full_screen_dialog){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.dialog_full_screen_progress);
                if (getWindow() != null){
                    getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT);
                }
            }
        };
        fullscreenProgressDialog.setCancelable(false);
        fullscreenProgressDialog.show();
    }

    public void showOkCustomDialog(String title, String description, View.OnClickListener onClickListener){
        Button okButton;
        TextView titleTextView;
        TextView descriptionTextView;

        Dialog dialog = new Dialog(context, R.style.AlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_ok);

        titleTextView = (TextView) dialog.findViewById(R.id.textview_ok_dialog_title);
        descriptionTextView = (TextView) dialog.findViewById(R.id.textview_ok_dialog_description);
        okButton = (Button) dialog.findViewById(R.id.button_ok_dialog_cancel);

        if (titleTextView != null) {
            titleTextView.setText(title);
        }
        if (descriptionTextView != null) {
            descriptionTextView.setText(description);
        }
        if (okButton != null) {
            okButton.setOnClickListener(onClickListener);
        }
        if (dialog.getWindow() != null){
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
        }
        dialog.show();
    }

    public void dismissFullScreenProgressDialog(){
        if (fullscreenProgressDialog != null){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fullscreenProgressDialog.dismiss();
                }
            }, 1000);
        }
    }
}