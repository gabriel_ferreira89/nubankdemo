package br.com.gabrielferreira.nubankdemo.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.manager.WSBaseManager;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;

/**
 * Created on 28/10/2016.
 */

public class WSNubankChargeback extends WSBaseManager {

    private final String TAG = getClass().getSimpleName();

    private static final String AUTHORIZATION = "Authorization";
    private static WSNubankChargeback instance;

    private WSNubankChargeback(Context context) {
        super(context);
    }

    public static synchronized WSNubankChargeback getInstance(Context context) {
        if (instance == null) {
            instance = new WSNubankChargeback(context.getApplicationContext());
        }
        return instance;
    }

    public void retrieveChargebackOperation(final ServerResponseManager serverResponseManager,
                                    String URL) {
        if(Utils.isConnected(context)) {
            Ion.with(context)
                    .load(GET, URL)
                    .setTimeout(Constants.WS_DEFAULT_TIMEOUT)
                    .asJsonObject()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<JsonObject>>() {
                        @Override
                        public void onCompleted(Exception e, Response<JsonObject> response) {
                            ResponseError error = checkErrors(e, response);
                            if (error == null) {
                                JsonObject result = response.getResult();
                                Gson gson = Utils.getStandardGsonBuilder().create();
                                Log.d(TAG, "chargeback Operation");
                                serverResponseManager.onSuccess(gson.fromJson(result, Chargeback.class));
                            }else{
                                serverResponseManager.onError(error);
                            }
                        }
                    });
        } else if (serverResponseManager != null) {
            serverResponseManager.onError(getConnectionError());
        }
    }

    public void postLockCardOperation(final ServerResponseManager serverResponseManager,
                                    String URL) {
        if(Utils.isConnected(context)) {
            Ion.with(context)
                    .load(POST, URL)
                    .setTimeout(Constants.WS_DEFAULT_TIMEOUT)
                    .asJsonObject()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<JsonObject>>() {
                        @Override
                        public void onCompleted(Exception e, Response<JsonObject> response) {
                            ResponseError error = checkErrors(e, response);
                            if (error == null) {
                                JsonObject result = response.getResult();
                                String resultString = result.get(Constants.LINK_STATUS).getAsString();
                                Log.d(TAG, "un/lockCard Operation");
                                serverResponseManager.onSuccess(resultString.toLowerCase().contains(Constants.LINK_STATUS_OK));
                            }else{
                                serverResponseManager.onError(error);
                            }
                        }
                    });
        } else if (serverResponseManager != null) {
            serverResponseManager.onError(getConnectionError());
        }
    }

    public void postChargebackOperation(final ServerResponseManager serverResponseManager,
                                    String URL, Chargeback chargeback) {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        if(Utils.isConnected(context)) {
            Ion.with(context)
                    .load(POST, URL)
                    .setTimeout(Constants.WS_DEFAULT_TIMEOUT)
                    .setJsonObjectBody(gson.toJsonTree(chargeback).getAsJsonObject())
                    .asJsonObject()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<JsonObject>>() {
                        @Override
                        public void onCompleted(Exception e, Response<JsonObject> response) {
                            ResponseError error = checkErrors(e, response);
                            if (error == null) {
                                Log.d(TAG, "chargeback Operation");
                                serverResponseManager.onSuccess(null);
                            }else{
                                serverResponseManager.onError(error);
                            }
                        }
                    });
        } else if (serverResponseManager != null) {
            serverResponseManager.onError(getConnectionError());
        }
    }
}
