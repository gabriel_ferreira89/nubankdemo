package br.com.gabrielferreira.nubankdemo.activity.chargeback;


import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.activity.base.BaseDialogFragment;
import br.com.gabrielferreira.nubankdemo.activity.main.MainContract;
import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;
import br.com.gabrielferreira.nubankdemo.model.Link;
import br.com.gabrielferreira.nubankdemo.model.ReasonDetail;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ChargebackDialogFragment extends BaseDialogFragment implements MainContract.View{

    public static final String TAG = "ChargebackDialogFragment";

    @Bind(R.id.button_chargeback_dialog_cancel)
    Button cancelButton;
    @Bind(R.id.button_chargeback_dialog_contest)
    Button contextButton;
    @Bind(R.id.textview_chargeback_title)
    TextView titleTextView;
    @Bind(R.id.textview_chargeback_lock_card)
    TextView lockCardTextView;
    @Bind(R.id.switchcompat_chargeback_merchant)
    SwitchCompat merchantSwitchCompat;
    @Bind(R.id.switchcompat_chargeback_card_possession)
    SwitchCompat cardPossessionSwitchCompat;
    @Bind(R.id.edittext_chargeback_comment)
    EditText commentEditText;

    private MainContract.Presenter presenter;
    private Chargeback chargeback;
    private boolean isCardLocked = Constants.CHARGEBACK_CARD_INITIAL_STATE;

    public static ChargebackDialogFragment newInstance(Chargeback chargeback) {
        ChargebackDialogFragment fragment = new ChargebackDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_CHARGEBACK, chargeback);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chargeback_dialog, container, false);
        chargeback = (Chargeback) getArguments().getSerializable(Constants.EXTRA_CHARGEBACK);
        ButterKnife.bind(this, view);
        initView();
        initListeners();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAllFields();
    }

    private void initView() {
        if (chargeback == null){
            return;
        }
        cancelButton.setText(R.string.cancel);
        contextButton.setText(R.string.chargeback_contest);
        titleTextView.setText(chargeback.getTitle());
        setCardLocked(isCardLocked);
        merchantSwitchCompat.setText(chargeback.getReasonDetail(Constants.WS_FIELD_CHARGEBACK_MERCHANT).getTitle());
        merchantSwitchCompat.setTag(Constants.WS_FIELD_CHARGEBACK_MERCHANT);
        cardPossessionSwitchCompat.setText(chargeback.getReasonDetail(Constants.WS_FIELD_CHARGEBACK_CARD).getTitle());
        cardPossessionSwitchCompat.setTag(Constants.WS_FIELD_CHARGEBACK_CARD);
        commentEditText.setHint(Utils.HtmlLtoSpanned(chargeback.getCommentHint()));
        if (chargeback.isAutoblock()){
            postLockCard(true);
        }
    }

    private void initListeners() {
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkAllFields();
            }
        });
    }

    private void postLockCard(final boolean blockCard) {
        Link blockCardLink;
        if (blockCard){
            blockCardLink = chargeback.getLinkMap().get(Constants.LINK_BLOCK_CARD);
        }else{
            blockCardLink = chargeback.getLinkMap().get(Constants.LINK_UNBLOCK_CARD);
        }
        if (blockCardLink != null){
            presenter.executeLockCard(new ServerResponseManager() {
                @Override
                public void onSuccess(Object data) {
                    if (data instanceof Boolean && (Boolean)data){
                        setCardLocked(blockCard);
                    }else{
                        setCardLocked(!blockCard);
                    }
                }
                @Override
                public void onError(ResponseError data) {
                    //do some internal action in case of error
                }
            }, chargeback.getLinkMap().get(Constants.LINK_BLOCK_CARD).getHref());
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void setCardLocked(boolean bool){
        if (bool){
            isCardLocked = true;
            lockCardTextView.setText(R.string.chargeback_locked_card);
            lockCardTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_chargeback_lock, 0, 0, 0);
        }else{
            isCardLocked = false;
            lockCardTextView.setText(R.string.chargeback_unlocked_card);
            lockCardTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_chargeback_unlock, 0, 0, 0);
        }
    }

    @OnClick(R.id.textview_chargeback_lock_card)
    void onLockCardTextViewClick(){
        postLockCard(!isCardLocked);
    }

    @OnClick(R.id.button_chargeback_dialog_cancel)
    void onCancelButtonClick(){
        dismiss();
    }

    @OnClick(R.id.button_chargeback_dialog_contest)
    void onConstestButtonClick(){
        Chargeback chargeback = new Chargeback();
        chargeback.setComment(commentEditText.getText().toString());
        List<ReasonDetail> reasonDetailList = new ArrayList<>();
        reasonDetailList.add(new ReasonDetail(String.valueOf(merchantSwitchCompat.getTag()), String.valueOf(merchantSwitchCompat.isChecked())));
        reasonDetailList.add(new ReasonDetail(String.valueOf(cardPossessionSwitchCompat.getTag()), String.valueOf(cardPossessionSwitchCompat.isChecked())));
        chargeback.setReasonDetails(reasonDetailList);
        Link link = this.chargeback.getLinkMap().get(Constants.LINK_SELF);
        if (link!= null){
            presenter.executeChargeback(chargeback, link.getHref());
        }
    }

    @OnCheckedChanged(R.id.switchcompat_chargeback_card_possession)
    void onCardPossessionCheckedChange(){
        if (cardPossessionSwitchCompat.isChecked()){
            cardPossessionSwitchCompat.setTextColor(Utils.getColor(getActivity(), R.color.nubank_green));
        }else{
            cardPossessionSwitchCompat.setTextColor(Utils.getColor(getActivity(), R.color.nubank_black));
        }
        checkAllFields();
    }

    @OnCheckedChanged(R.id.switchcompat_chargeback_merchant)
    void onMerchantCheckedChange(){
        if (merchantSwitchCompat.isChecked()){
            merchantSwitchCompat.setTextColor(Utils.getColor(getActivity(), R.color.nubank_green));
        }else{
            merchantSwitchCompat.setTextColor(Utils.getColor(getActivity(), R.color.nubank_black));
        }
        checkAllFields();
    }

    /**
     * Unblock in case of change of business rules
     */

    private void checkAllFields() {
        if (/*!merchantSwitchCompat.isChecked()
                || !cardPossessionSwitchCompat.isChecked()
                ||*/ "".equals(commentEditText.getText().toString())){
            contextButton.setEnabled(false);
            return;
        }
        contextButton.setEnabled(true);
    }
}
