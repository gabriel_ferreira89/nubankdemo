package br.com.gabrielferreira.nubankdemo.model;

import java.util.List;

public class ResponseError {

    private List<ServerError> serverErrorList;
    private Integer statusCode;

    public List<ServerError> getServerErrorList() {
        return serverErrorList;
    }

    public void setServerErrorList(List<ServerError> serverErrorList) {
        this.serverErrorList = serverErrorList;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
