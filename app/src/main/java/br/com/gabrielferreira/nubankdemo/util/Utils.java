package br.com.gabrielferreira.nubankdemo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import br.com.gabrielferreira.nubankdemo.NubankdemoApplication;
import br.com.gabrielferreira.nubankdemo.model.Chargeback;
import br.com.gabrielferreira.nubankdemo.model.HalResource;
import br.com.gabrielferreira.nubankdemo.model.Link;
import br.com.gabrielferreira.nubankdemo.model.Notice;

/**
 * Created on 25/10/2016.
 */

public class Utils {

    public static String getMockedJsonContent(int jsonId) throws IOException, JSONException {
        BufferedReader bufferedReader = null;
        try {
            InputStream inStream = NubankdemoApplication.getInstance().getResources().openRawResource(jsonId);

            BufferedInputStream bufferedStream = new BufferedInputStream(
                    inStream);
            InputStreamReader reader = new InputStreamReader(bufferedStream);
            bufferedReader = new BufferedReader(reader);
            StringBuilder builder = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null) {
                builder.append(line);
                line = bufferedReader.readLine();
            }
            return builder.toString();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }

    public static HashMap<String, Link> jsonToMap(JsonObject json) throws JSONException {
        HashMap<String, Link> retMap = new HashMap<>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static HashMap<String, Link> toMap(JsonObject object) throws JSONException {
        HashMap<String, Link> map = new HashMap<>();

        Iterator<String> keysItr = new JSONObject(object.toString()).keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JsonObject) {
//                value = toMap((JsonObject) value);
                map.put(key, new Gson().fromJson(value.toString(), Link.class));
            }
        }
        return map;
    }

    public static GsonBuilder getStandardGsonBuilder(){
        GsonBuilder gsonBldr = new GsonBuilder();
        gsonBldr.registerTypeAdapter(HalResource.class, new HalResource.HalResourceDeserializerFromJson());
        gsonBldr.registerTypeAdapter(Notice.class, new Notice.NoticeDeserializerFromJson());
        gsonBldr.registerTypeAdapter(Chargeback.class, new Chargeback.ChargebackDeserializerFromJson());
       return gsonBldr;
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static Object getMockedJson(int id, Class c) throws IOException, JSONException {
        return getStandardGsonBuilder().create().fromJson(getMockedJsonContent(id), c);
    }

    public static Spanned HtmlLtoSpanned(String text){
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);    // for 24 api and more
        } else {
            //noinspection deprecation
            return Html.fromHtml(text); // or for older api
        }
    }

    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(id);
        } else {
            //noinspection deprecation
            return context.getResources().getColor(id);
        }
    }
}
