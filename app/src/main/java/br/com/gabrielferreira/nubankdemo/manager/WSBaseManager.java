package br.com.gabrielferreira.nubankdemo.manager;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import br.com.gabrielferreira.nubankdemo.R;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.model.ServerError;
import br.com.gabrielferreira.nubankdemo.util.Constants;


public class WSBaseManager {

    protected static final String ERRORS_KEY = "errors";
    protected static final String PUT = "PUT";
    protected static final String POST = "POST";
    protected static final String GET = "GET";
    protected static final String DELETE = "DELETE";
    protected static final String AUTHORIZATION = "Authorization";

    protected Context context;

    public WSBaseManager(Context context) {
        this.context = context;
    }

    protected ResponseError checkErrors(Exception e, Response<JsonObject> response) {

        ResponseError responseError = new ResponseError();
        List<ServerError> errorList = new ArrayList<>();
        int headerCode = 0;

        if (response != null && response.getHeaders() != null) {
            headerCode = response.getHeaders().code();
        }

        if (e != null) {
            ServerError error = new ServerError();
            if (e instanceof TimeoutException) {
                responseError.setStatusCode(Constants.WSHeaderCode.TIME_OUT_CODE);
                error.setCode(Constants.WSHeaderCode.TIME_OUT_CODE);
                error.setMessage(context.getResources().getString(R.string.no_connection_error));
                errorList.add(error);
            } else {
                responseError.setStatusCode(Constants.WSHeaderCode.GENERIC_EXCEPTION);
                error.setCode(Constants.WSHeaderCode.GENERIC_EXCEPTION);
                error.setMessage(context.getResources().getString(R.string.generic_error));
                errorList.add(error);
            }
        } else if (headerCode == Constants.WSHeaderCode.SUCCESS_CODE || headerCode == Constants.WSHeaderCode.OBJECT_CREATED_CODE) {
            return null;
        } else if (response != null && response.getResult() != null && response.getResult().get(ERRORS_KEY) != null) {
            JsonObject result = response.getResult();

            responseError.setStatusCode(headerCode);

            // array com um elemento e convertido pra jsonobject pelo gson
            if (result.get(ERRORS_KEY) instanceof JsonObject) {
                JsonObject singleError = result.getAsJsonObject(ERRORS_KEY);
                ServerError serverError = new ServerError(singleError);
                errorList.add(serverError);
            } else if (result.get(ERRORS_KEY) instanceof JsonArray) {
                JsonArray errors = result.get(ERRORS_KEY).getAsJsonArray();
                for (int i = 0; i < errors.size(); i++) {
                    if (errors.get(i) != null && errors.get(i) instanceof JsonObject) {
                        JsonObject singleError = errors.get(i).getAsJsonObject();
                        ServerError serverError = new ServerError(singleError);
                        errorList.add(serverError);
                    }
                }
            }
        }
        responseError.setServerErrorList(errorList);
        return responseError;
    }

    protected ResponseError getConnectionError() {
        if (context != null) {
            ResponseError error = new ResponseError();
            error.setStatusCode(Constants.WSHeaderCode.TIME_OUT_CODE);
            List<ServerError> listError = new ArrayList<>();
            listError.add(new ServerError(Constants.WSErrorCode.NO_CONNECTION, context.getResources().getString(R.string.no_connection_error)));
            error.setServerErrorList(listError);
            return error;
        }
        return null;
    }
}
