package br.com.gabrielferreira.nubankdemo.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.manager.WSBaseManager;
import br.com.gabrielferreira.nubankdemo.model.HalResource;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;

/**
 * Created on 28/10/2016.
 */

public class WSNubank extends WSBaseManager{

    private final String TAG = getClass().getSimpleName();

    private static final String AUTHORIZATION = "Authorization";
    private static WSNubank instance;

    private WSNubank(Context context) {
        super(context);
    }

    public static synchronized WSNubank getInstance(Context context) {
        if (instance == null) {
            instance = new WSNubank(context.getApplicationContext());
        }
        return instance;
    }
    public void retrieveMain(final ServerResponseManager callback,
                                    String URL) {

        if(Utils.isConnected(context)) {
            Ion.with(context)
                    .load(GET, URL)
                    .setTimeout(Constants.WS_DEFAULT_TIMEOUT)
                    .asJsonObject()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<JsonObject>>() {
                        @Override
                        public void onCompleted(Exception e, Response<JsonObject> response) {
                            ResponseError error = checkErrors(e, response);
                            if (error == null) {
                                JsonObject result = response.getResult();
                                Log.d(TAG, "retrieve Main");
                                callback.onSuccess(Utils.getStandardGsonBuilder().create().fromJson(result, HalResource.class));
                            }else{
                                callback.onError(error);
                            }
                        }
                    });
        } else if (callback != null) {
            callback.onError(getConnectionError());
        }
    }
}
