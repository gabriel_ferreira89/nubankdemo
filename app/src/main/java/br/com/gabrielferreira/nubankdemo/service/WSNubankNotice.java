package br.com.gabrielferreira.nubankdemo.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import br.com.gabrielferreira.nubankdemo.listener.ServerResponseManager;
import br.com.gabrielferreira.nubankdemo.manager.WSBaseManager;
import br.com.gabrielferreira.nubankdemo.model.Notice;
import br.com.gabrielferreira.nubankdemo.model.ResponseError;
import br.com.gabrielferreira.nubankdemo.util.Constants;
import br.com.gabrielferreira.nubankdemo.util.Utils;

/**
 * Created on 28/10/2016.
 */

public class WSNubankNotice extends WSBaseManager{

    private final String TAG = getClass().getSimpleName();

    private static final String AUTHORIZATION = "Authorization";
    private static WSNubankNotice instance;

    private WSNubankNotice(Context context) {
        super(context);
    }

    public static synchronized WSNubankNotice getInstance(Context context) {
        if (instance == null) {
            instance = new WSNubankNotice(context.getApplicationContext());
        }
        return instance;
    }

    public void retrieveNotice(final ServerResponseManager serverResponseManager,
                               String URL) {

        if(Utils.isConnected(context)) {
            Ion.with(context)
                    .load(GET, URL)
                    .setTimeout(Constants.WS_DEFAULT_TIMEOUT)
                    .asJsonObject()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<JsonObject>>() {
                        @Override
                        public void onCompleted(Exception e, Response<JsonObject> response) {
                            ResponseError error = checkErrors(e, response);
                            if (error == null) {
                                JsonObject result = response.getResult();
                                Log.d(TAG, "retrieveNotice Operation");
                                serverResponseManager.onSuccess(Utils.getStandardGsonBuilder().create().fromJson(result, Notice.class));
                            }else{
                                serverResponseManager.onError(error);
                            }
                        }
                    });
        } else if (serverResponseManager != null) {
            serverResponseManager.onError(getConnectionError());
        }
    }
}
