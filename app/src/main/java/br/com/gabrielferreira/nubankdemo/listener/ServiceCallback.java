package br.com.gabrielferreira.nubankdemo.listener;

import br.com.gabrielferreira.nubankdemo.model.ResponseError;

public interface ServiceCallback<T> {

    void onCompleted(ResponseError error, T result);
}
